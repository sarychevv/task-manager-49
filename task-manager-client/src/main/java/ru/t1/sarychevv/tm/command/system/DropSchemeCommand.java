package ru.t1.sarychevv.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.dto.request.system.DropSchemeRequest;
import ru.t1.sarychevv.tm.dto.response.system.DropSchemeResponse;

public class DropSchemeCommand extends AbstractCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Drop scheme.";
    }

    @Override
    @NotNull
    public String getName() {
        return "drop";
    }

    @Override
    public void execute() {
        System.out.println("[DROP SCHEME]");
        @NotNull final DropSchemeResponse response = getServiceLocator().
                getSystemEndpoint().dropScheme(new DropSchemeRequest(getToken()));
        System.out.println(response);
    }
}
