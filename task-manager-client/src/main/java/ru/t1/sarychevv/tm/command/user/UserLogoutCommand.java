package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "User logout.";
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logout(request);
    }

}
