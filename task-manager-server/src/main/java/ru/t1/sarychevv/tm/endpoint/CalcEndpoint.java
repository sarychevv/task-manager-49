package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.ICalcEndpoint;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ICalcEndpoint")
public class CalcEndpoint extends AbstractEndpoint implements ICalcEndpoint {

    public CalcEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

}
