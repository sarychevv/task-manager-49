package ru.t1.sarychevv.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.dto.IUserDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.ExistsEmailException;
import ru.t1.sarychevv.tm.exception.user.ExistsLoginException;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.repository.dto.UserDTORepository;
import ru.t1.sarychevv.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserDTOService extends AbstractDTOService<UserDTO, IUserDTORepository> implements IUserDTOService {

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final ITaskDTOService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserDTOService(@NotNull final IPropertyService propertyService,
                          @NotNull final IConnectionService connectionService,
                          @NotNull final IProjectDTOService projectService,
                          @NotNull final ITaskDTOService taskService) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    protected IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login,
                          @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login,
                          @Nullable final String password,
                          @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login,
                          @Nullable final String password,
                          @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return (repository.findByLogin(login) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            return (repository.findByEmail(email) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull UserDTO lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public @Nullable UserDTO removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        removeOne(user);
        return user;
    }

    @Override
    public @Nullable UserDTO setPassword(@Nullable final String id,
                                         @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @NotNull UserDTO unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @Nullable UserDTO updateUser(@Nullable final String id,
                                        @Nullable final String firstName,
                                        @Nullable final String lastName,
                                        @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

}

