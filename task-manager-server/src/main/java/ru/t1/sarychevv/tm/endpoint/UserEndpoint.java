package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.IUserEndpoint;
import ru.t1.sarychevv.tm.api.service.IAuthService;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.dto.response.user.*;
import ru.t1.sarychevv.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserDTOService getUserService() {
        return getServiceLocator().getUserDTOService();
    }

    @Override
    @NotNull
    @WebMethod
    public UserLockResponse lockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull UserDTO user = new UserDTO();
        try {
            user = getUserService().lockUserByLogin(login);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockResponse unlockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull UserDTO user = new UserDTO();
        try {
            user = getUserService().unlockUserByLogin(login);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveResponse removeUser(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable UserDTO user = new UserDTO();
        try {
            user = getUserService().removeByLogin(login);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final UserUpdateProfileRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final String lastName = request.getLastName();
        @NotNull final String firstName = request.getFirstName();
        @NotNull final String middleName = request.getMiddleName();
        @Nullable UserDTO user = new UserDTO();
        try {
            user = getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(@WebParam(name = REQUEST, partName = REQUEST)
                                                         @NotNull final UserChangePasswordRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable UserDTO user = new UserDTO();
        try {
            user = getUserService().setPassword(userId, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryResponse registryUser(@WebParam(name = REQUEST, partName = REQUEST)
                                             @NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull IAuthService authService = getServiceLocator().getAuthService();
        @Nullable UserDTO user = new UserDTO();
        try {
            user = authService.registry(login, password, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserRegistryResponse(user);
    }

}
