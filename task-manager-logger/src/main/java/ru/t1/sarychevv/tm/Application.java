package ru.t1.sarychevv.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.listener.EntityListener;
import ru.t1.sarychevv.tm.service.LoggerService;

import javax.jms.*;

public final class Application {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(@Nullable String[] args) {
        final LoggerService loggerService = new LoggerService();
        final EntityListener entityListener = new EntityListener(loggerService);
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue destination = session.createQueue(QUEUE);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
