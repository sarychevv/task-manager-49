package ru.t1.sarychevv.tm.listener;

import lombok.SneakyThrows;
import ru.t1.sarychevv.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EntityListener implements MessageListener {

    private final LoggerService loggerService;

    public EntityListener(final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        final TextMessage textMessage = (TextMessage) message;
        final String text = textMessage.getText();
        loggerService.log(text);
    }

}
